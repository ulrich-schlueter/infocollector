#!/usr/bin/env bash

set -x

oc get nodes -o json > json/nodes.json

oc get co -o json > json/clusteroperators.json

oc get clusterversion version -o json > json/clusterversion.json


RAWFOLDER="raw"
#Raws
name="ds"
echo "Command:oc get ds -A" > "$RAWFOLDER/$name.meta"
oc get ds -A > "$RAWFOLDER/$name.txt"

