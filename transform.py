#Imports from Jinja2
from jinja2 import Environment, FileSystemLoader
import json
import pprint
import os
import glob



#Load Jinja2 template
env = Environment(loader = FileSystemLoader('./'), trim_blocks=True, lstrip_blocks=True)

pp = pprint.PrettyPrinter(indent=4)


jsonfolder="json"
jsonfiles={'clusteroperators':'clusteroperators.json',
           'clusterversion':  'clusterversion.json',
	   'nodes': 'nodes.json'
	}

rawFilesFolder="raw"

templateFolder="j2"
targetFolder="html"
templates=["index.html","history.html","basics.html", "rawDisplays.html","specifics.html"]


data={}
data['title']="Primary"
data['json']={}
data['raw']={}
data['rawmeta']={}

for key,file in jsonfiles.items():
	with open (jsonfolder+"/"+file,"r") as f:
		jsondict=json.load(f)
		data['json'][key]=jsondict

directories = glob.glob(rawFilesFolder+"/*.txt")
for file in directories:   
	with open (file,"r") as f:
		rawText=f.read()
		data['raw'][file]=rawText
	metafile=file[:-4]+".meta"
	with open (metafile,"r") as f:
		rawText=f.read()
		data['rawmeta'][file]=rawText


#pp.pprint (data)
print ("data imported")

for t in templates:
	with open (targetFolder+"/"+t,"w") as f:
		template = env.get_template(templateFolder+"/"+t)
		f.write(template.render(data))

print ("file written")
